# Snakemake workflows for octopus runs

This repository contains an example of running many Octopus simulations using
[Snakemake](https://snakemake.github.io/), a workflow management system.

To install snakemake, use
```
pip install --user "snakemake>=7.32" 
```
We need at least the recent version 7.32 in which a bug has been fixed that affects us.

The convergence plot depends on postopus. It uses recent features not yet in the
main branch. You can install the needed version with
```
pip install --user git+https://gitlab.com/octopus-code/postopus.git@132-restructure-tests
```


## Configuring a workflow

Different workflows are contained in the `workflow_*` subfolder. They are
configured through different config files and template directories. Please see
a description in each subfolder.

In general, there are some config files in `config` that you can use as a
starting point.

In the config file, the template directory needs to be specified. Each template directory
should contain `gs` and `td` subdirectories, each with an `inp` file and potentially
some other files that are copied to the run directories.

There are two ways to parametrize the input files:

1. Template variables: Under `template_variables` in the config file, variables
can be listed that will be used for substitution in the input file template, in
which they need to be prefixed with a $ sign. As an example, one can choose different
systems by adding the following to the config file:
Edit `config/config.yaml` and for each variable that is to be changed in the template,
add a list of values of that variable, e.g.:
```yaml
template_variables:
    spacing:
     - 0.1
     - 0.2
```
In the template `inp` file, one would use:
```
Spacing = $spacing
```

2. Input variables: Under `input_variables` in the config file, variables can be listed
that will be added as extra lines to the input files, e.g. for
```yaml
input_variables:
    RegriddingInterpolationLevel:
      - regridding_linear
      - regridding_nearest_neighbor
```
the following will be added to the input file:
```
RegriddingInterpolationLevel = regridding_linear
```
and accordingly for the other values.


## Running a workflow

The runs can be started on Ada using
```
snakemake --slurm --configfile config/config.yaml --profile profiles/ada all
```
which will submit all jobs to the gpu partition.  You need to specify the
environment variable `$OCT_HOME` pointing to the installation of Octopus. On
Ada, this can be achieved by simply loading a suitable Octopus module.
By specifying `--profile profiles/ada_cpu`, the jobs will run on the cpu partition.

In addition, the option `-n` can be used for a dry run to see which runs would be started.

The input files and the convergence plot will be created on the login node
(they are defined as local jobs).
