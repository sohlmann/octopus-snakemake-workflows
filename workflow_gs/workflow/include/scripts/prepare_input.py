from string import Template
import os
import glob
import shutil

def mkdir_p(path):
    os.makedirs(path, exist_ok=True)

mkdir_p(snakemake.params["folder"])

# first copy all files
file_list = glob.glob(os.path.join("template", snakemake.params["mode"] , "*"))
for file in file_list:
    if file == "inp":
        continue
    shutil.copy(file, snakemake.params["folder"])

# now generate inp file
# read template file
filename_template = os.path.join("template", snakemake.params["mode"] , "inp")
with open(filename_template, 'r') as f_template:
    template = Template(f_template.read())

# filename of input file given by snakemake
filename_input = snakemake.output[0]
# get combinations from wildcards
folders = snakemake.wildcards[0].split("/")
combinations = {folder.split("-")[0]: "-".join(folder.split("-")[1:])
                for folder in folders}
# do template substitution for these parameters
with open(filename_input, 'w') as f_input:
    f_input.write(template.substitute(**combinations))

