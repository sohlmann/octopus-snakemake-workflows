CalculationMode = td

%RestartOptions
  restart_gs | "../gs/restart"
%

BoxShape = parallelepiped
PeriodicDimensions = 3

# hexagonal unit cell
aa = 3.0763 * angstrom
cc = 5.0480 * angstrom

%LatticeParameters
 aa | aa | cc
 90 | 90 | 120
%

# 4 atoms per unit cell, wurtzite structure
# use Wyckoff position 2b for both C and Si
# Wyckoff position from https://www.cryst.ehu.es/cgi-bin/cryst/programs/nph-wp-list
# z parameters from Nomad Encyclopedia (https://nomad-lab.eu/prod/rae/encyclopedia/#/material/JUgOnGsqxZgEDuEanJGH6SglpCol/structure)
zSi = 0.0
#zC = 0.38
zC = 3/8
%ReducedCoordinates
  "Si" | 1/3 | 2/3 | zSi
  "Si" | 2/3 | 1/3 | zSi + 1/2
  "C"  | 1/3 | 2/3 | zC
  "C"  | 2/3 | 1/3 | zC + 1/2
%

# treatment of atoms
PseudopotentialSet = pseudodojo_pbesol
# these are PBE SOL pseudopotentials from pseudodojo, regenerated with (semi-)core states
%Species
 "C" | species_pseudo | file | "C.upf"
 "Si" | species_pseudo | file | "Si.upf"
%


# parameters to be converged

#Spacing = 0.16
Spacing = $spacing * angstrom

%KPointsGrid
  6 | 6 | 10
%
KPointsUseSymmetries = yes
%SymmetryBreakDir
 1 | 0 | 0
%

# get some unoccupied bands as well
#ExtraStates = 4

# Output options
%Output
 density | xcrysden
%

UnitsOutput = ev_angstrom

TDPropagator = aetrs
TDTimeStep = 0.4 * Spacing^2
TDPropagationTime = 10/eV

%GaugeVectorField
 0.1 | 0 | 0
%
