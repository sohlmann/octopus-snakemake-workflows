# workflow_gs

The workflow in this folder is adapted to run GS runs for different parameters
and potentially one TD run and also a spectrum for each GS run.

- `config/config_gs_comparison.yaml`, `template_gs_comparison`: run GS
  simulations for different systems; can be used to analyze eigensolvers and
  other options affecting the GS convergence 
- `config/config_convergence_spectra.yaml`, `template_convergence_spectra`: run
  a series GS + TD + spectrum for all parameters specified in the config file;
  can be used to analyze how the spectrum converges with spacing
