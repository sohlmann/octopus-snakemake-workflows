#!/usr/bin/env python
import postopus
from postopus.datacontainers.util.output_collector import OutputCollector
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


def combine_dataframes(f):
    n = postopus.nestedRuns()
    # runs are in runs subfolder, ignore gs folder
    return get_dataframe(n['runs'].apply(f)).droplevel("gs")


def get_last_iteration(df):
    return df.groupby(level=list(range(len(df.index.names)-1)))\
        .tail(1).sort_index()
        #.tail(1).droplevel(-1).sort_index()


def get_dataframe(runs):
    try:
        dict = {}
        for k, df in runs.items():
            if isinstance(df, pd.DataFrame):
                key = k.split("__")[0]
                label = key.split("-")[0]
                value = "-".join(key.split("-")[1:])
                index_name = df.index.name
                dict[value] = df
            else:
                raise TypeError
        df = pd.concat(dict, names=[label, index_name])
    except TypeError:
        dict = {}
        for k, v in runs.items():
            key = k.split("__")[0]
            label = key.split("-")[0]
            value = "-".join(key.split("-")[1:])
            dict[value] = get_dataframe(v)
            index_names = dict[value].index.names
        df = pd.concat(dict, names=[label]+index_names)
    return df


def plot_convergence():
    df = combine_dataframes(lambda run: run.default.scf.convergence)
    # todo: make dataframes out of the float
    df_spacing = combine_dataframes(
        lambda run: pd.DataFrame.from_dict({
            "Spacing": float(OutputCollector(run.path)._config.fields_raw["Spacing"])
        }, orient="index").T).droplevel(-1)
    df = df.join(df_spacing)
    last = get_last_iteration(df).reset_index("#iter")
    with PdfPages("convergence.pdf") as pdf:
        width = 5
        # plot comparison of parameter combinations for each system
        grouped = df.groupby("system")
        # filter indices for "system" and "#iter"
        new_indices = [name for name in df.index.names
                       if name not in ["system", "#iter"]]
        for k, v in grouped:
            f, ax = plt.subplots(1, 1, figsize=(width, width*0.6), sharex=True)
            ax.set_title(k)
            for k2, v2 in v.groupby(new_indices):
                ax.plot(v2.index.get_level_values("#iter"), v2["energy"], label=k2)
            ax.legend()
            ax.set_ylabel("Energy")
            ax.set_xlabel("Iteration number")
            f.tight_layout()
            pdf.savefig()
            plt.close()

        new_indices = [name for name in last.index.names
                       if name not in ["step"]]
        for k, v in last.groupby(new_indices):
            f, ax = plt.subplots(1, 1, figsize=(width, width*0.6), sharex=True)
            ax.set_title(k)
            ax.plot(v["Spacing"], v["energy"], "x-")
            ax.set_ylabel("Energy")
            ax.set_xlabel("Spacing [b]")
            f.tight_layout()
            pdf.savefig()
            plt.close()

        # last["#iter"].groupby(df.index.names[:-2]).boxplot()
        # pdf.savefig()
        # plt.close()


if __name__ == "__main__":
    plot_convergence()
