# check if converged
# if not, add spacing -0.1 to config["input_variables"]["Spacing"]
import postopus
from pathlib import Path
import numpy as np


# get combinations from wildcards
def get_input_variables():
    folders = snakemake.params.folder.split("/")
    input_variables = {}
    for folder in folders:
        prefix = folder[:-3]
        suffix = folder[-3:]
        if suffix == "__i":
            input_variables[prefix.split("-")[0]] =  "-".join(prefix.split("-")[1:])
    return input_variables

spacing = np.float64(get_input_variables()["Spacing"])

def get_energy(folder):
    run = postopus.Run(Path(folder))
    return run.default.scf.convergence.energy.iloc[-1]

current = get_energy(snakemake.params.folder)

energies = []
paths = (Path(snakemake.params.folder)/".."/"..").glob("*")
for path in paths:
    if path/"gs" == snakemake.params.folder:
        continue
    energies.append(get_energy(path/"gs"))

def add_spacing():
    snakemake.config["input_variables"]["Spacing"].append(
        spacing-np.float64(snakemake.config["spacing_step"]))

energies = np.array(energies)
diff = np.abs(energies - current)
if len(energies) == 0:
    add_spacing()
elif np.all(diff < np.float64(snakemake.config["threshold"])):
    add_spacing()

iteration = len(list(paths))
with open(snakemake.output[0], "w") as f:
    f.write(f"{iteration}\n")
