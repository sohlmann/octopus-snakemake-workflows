from string import Template
import os
import glob
import shutil

def mkdir_p(path):
    os.makedirs(path, exist_ok=True)

mkdir_p(snakemake.params["folder"])

# first copy all files
file_list = glob.glob(os.path.join(snakemake.config["template"],
                                   snakemake.params["mode"], "*"))
for file in file_list:
    if file == "inp":
        continue
    shutil.copy(file, snakemake.params["folder"])

# now generate inp file
# read template file
filename_template = os.path.join(snakemake.config["template"],
                                 snakemake.params["mode"], "inp")
with open(filename_template, 'r') as f_template:
    template = Template(f_template.read())

# filename of input file given by snakemake
filename_input = snakemake.output[0]
# get combinations from wildcards
folders = snakemake.wildcards[0].split("/")
template_variables = {}
input_variables = {}
for folder in folders:
    prefix = folder[:-3]
    suffix = folder[-3:]
    if suffix == "__t":
        template_variables[prefix.split("-")[0]] = "-".join(prefix.split("-")[1:])
    elif suffix == "__i":
        input_variables[prefix.split("-")[0]] = "-".join(prefix.split("-")[1:])

spacing = float(snakemake.config["initial_spacing"]) - \
    float(snakemake.config["spacing_step"])*float(snakemake.params["iteration"])
#input_variables["Spacing"] = f"{spacing} * angstrom"
input_variables["Spacing"] = f"{spacing}"

with open(filename_input, 'w') as f_input:
    # do template substitution for the template variables
    substituted = template.substitute(**template_variables) + "\n"
    # add one line for each input variable
    for key, value in input_variables.items():
        substituted += key + " = " + value + "\n"
    f_input.write(substituted)
