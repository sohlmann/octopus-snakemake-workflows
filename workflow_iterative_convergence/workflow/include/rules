localrules: gs_inp, td_inp

rule gs_inp:
    input:
        f"{config['template']}/gs/inp"
    output:
        "{folder}/step-{i}/gs/inp"
    params:
        folder="{folder}/step-{i}/gs",
        mode="gs",
        iteration="{i}"
    script:
        "scripts/prepare_input.py"

checkpoint gs:
    input:
        "{folder}/gs/inp"
    output:
        "{folder}/gs/exec/oct-status-finished",
# add this line for wokrflows with TD simulations where RestartWrite is enabled
#        "{folder}/gs/restart/gs/states"
    params:
        folder="{folder}/gs",
        path=os.environ["OCT_HOME"]
    shell:
        "cd {params.folder} && OMP_NUM_THREADS={resources.cpus_per_task} {resources.mpi} -n {resources.tasks} {params.path}/bin/octopus > log 2>&1"

checkpoint gs_converged:
    input:
        "{folder}/gs/exec/oct-status-finished",
    output:
        "{folder}/gs/iteration",
    params:
        folder="{folder}/gs",
    script:
        "scripts/check_convergence.py"

rule td_inp:
    input:
        f"{config['template']}/td/inp"
    output:
        "{folder}/td/inp"
    params:
        folder="{folder}/td",
        mode="td"
    script:
        "scripts/prepare_input.py"

rule td:
    input:
        "{folder}/td/inp",
        "{folder}/gs/restart/gs/states"
    output:
        "{folder}/td/exec/oct-status-finished",
        "{folder}/td/td.general/energy",
        "{folder}/td/td.general/gauge_field",
        "{folder}/td/td.general/multipoles"
    params:
        folder="{folder}/td",
        path=os.environ["OCT_HOME"]
    shell:
        "cd {params.folder} && OMP_NUM_THREADS={resources.cpus_per_task} {resources.mpi} -n {resources.tasks} {params.path}/bin/octopus > log 2>&1"

rule dielectric_function:
    input:
        "{folder}/td/td.general/energy",
        "{folder}/td/td.general/gauge_field",
        "{folder}/td/td.general/multipoles",
        "{folder}/td/inp"
    output:
        "{folder}/td/td.general/dielectric_function",
        "{folder}/td/td.general/inverse_dielectric_function",
        "{folder}/td/td.general/chi"
    params:
        folder="{folder}/td"
    shell:
        "cd {params.folder} && oct-dielectric-function > log_dielectric 2>&1"
