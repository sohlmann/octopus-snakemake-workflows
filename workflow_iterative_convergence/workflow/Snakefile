configfile: "config/config.yaml"

include: "include/python_functions"
include: "include/rules"

localrules: all, all_inp, plot


def get_energy(folder):
    from pathlib import Path
    import postopus
    run = postopus.Run(Path(folder))
    return run.default.scf.convergence.energy.iloc[-1]

def is_converged(run_folder, iteration):
    from pathlib import Path
    import numpy as np
    if iteration == 0:
        return False
    try:
        energy_previous = get_energy(Path(run_folder)/f"step-{iteration-1}/gs")
        energy_current = get_energy(Path(run_folder)/f"step-{iteration}/gs")
    except:
        return False
    if np.abs(energy_current - energy_previous) < float(config["threshold"]):
        return True
    else:
        return False

def get_iteration_path(run, iteration, path):
    return run/f"step-{iteration}"/path

def output_files_gs(wildcards):
    path = "gs/exec/oct-status-finished"
    run_list = output_files(config["template_variables"],
                            config["input_variables"], ".")
    file_list = []
    for run in run_list:
        for iteration in range(0, 8):
            # always need to run iterations 0 and 1
            if iteration < 2:
                file_list.append(get_iteration_path(run, iteration, path))
            # run the current iteration only if the previous one exists and
            # if it is not converged (w.r.t. to the next-to-previous one)
            elif get_iteration_path(run, iteration-1, path).exists() and \
                    not is_converged(run, iteration-1):
                file_list.append(get_iteration_path(run, iteration, path))
    return file_list

def input_files_gs(wildcards):
    path = "/gs/inp"
    return output_files(config["template_variables"],
                        config["input_variables"], path)

envvars:
  "OCT_HOME"

rule all:
    input:
        output_files_gs

rule all_inp:
    input:
        input_files_gs

rule plot:
    input:
        output_files_gs
    output:
        "convergence.pdf"
    script:
        "include/scripts/plot_iterative_convergence.py"
        #"include/scripts/plot_convergence.py"
