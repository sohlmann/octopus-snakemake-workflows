# workflow_iterative_convergence

This folder contains workflows for iterative convergence of different GS runs,
e.g., to converge the spacing individually for different systems. This way of
using the workflow differs from other workflows because runs are added on the
fly, depending on the convergence properties.

- `config/config_iterative_convergence.yaml`, `template_gs_comparison`: run a
  convergence with respect to spacing for a list of systems and parameters;
  convergence parameters can be controlled from the config file
