# workflow_gs_plus_td

This folder contains workflows for parameters that affect GS and TD runs
separately, e.g., different GS runs for different spacing, but then different
TD runs for each GS run.

The config file can be parametrized as described in the README, but the two
blocks of template and input variables need to be specified for gs and td runs.
The workflow system will then create inp files in a folder hierarchy below a
folder `runs` for all combinations of the GS runs and additionally for each GS
runs all combinations of TD runs. In order to find the correct restart files,
the option `RestartOptions` needs to be set; for an example, see the template
file `template_tddft_maxwell/td/inp`. Please be aware that the leading `..` in
the path for the restart folder is only necessary for multisystem runs.


- `config/config_tddft_maxwell.yaml`, `template_tddft_maxwell`: run a series of
  TDDFT+Maxwell calculations for different ground state parameters (e.g.,
  spacing)
