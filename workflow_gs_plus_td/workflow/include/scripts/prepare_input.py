from string import Template
import os
import glob
import shutil

def mkdir_p(path):
    os.makedirs(path, exist_ok=True)

mkdir_p(snakemake.params["folder"])

# first copy all files
file_list = glob.glob(os.path.join(snakemake.config["template"],
                                   snakemake.params["mode"] , "*"))
for file in file_list:
    if file == "inp":
        continue
    shutil.copy(file, snakemake.params["folder"])

# now generate inp file
# read template file
filename_template = os.path.join(snakemake.config["template"],
                                 snakemake.params["mode"] , "inp")
with open(filename_template, 'r') as f_template:
    template = Template(f_template.read())

# filename of input file given by snakemake
filename_input = snakemake.output[0]
# get combinations from output file without filename
folders = snakemake.output[0].split("/")[:-1]
template_variables = {}
input_variables = {}
relative = []
for folder in folders:
    relative += [".."]
    if folder == "gs" or folder == "td":
        continue
    prefix = folder[:-3]
    suffix = folder[-3:]
    if suffix == "__t":
        template_variables[prefix.split("-")[0]] =  "-".join(prefix.split("-")[1:])
    elif suffix == "__i":
        input_variables[prefix.split("-")[0]] =  "-".join(prefix.split("-")[1:])

# get gs folder for td runs, add to template variables
try:
    template_variables["gs_folder"] = "/".join(relative) + "/" + snakemake.wildcards.gs_folder
except:
    pass

with open(filename_input, 'w') as f_input:
    # do template substitution for the template variables
    substituted = template.substitute(**template_variables) + "\n"
    # add one line for each input variable
    for key, value in input_variables.items():
        substituted += key + " = " + value + "\n"
    f_input.write(substituted)
