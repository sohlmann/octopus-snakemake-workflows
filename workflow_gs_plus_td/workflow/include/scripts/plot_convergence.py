#!/usr/bin/env python
import postopus
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


def combine_dataframes(f):
    n = postopus.nestedRuns()
    # runs are in runs subfolder, ignore gs folder
    return get_dataframe(n['runs'].apply(f)).droplevel("gs")


def get_last_iteration(df):
    return df.groupby(level=list(range(len(df.index.names)-1)))\
        .tail(1).sort_index()
        #.tail(1).droplevel(-1).sort_index()


def get_dataframe(runs):
    try:
        dict = {}
        for k, df in runs.items():
            if not isinstance(df, pd.DataFrame):
                raise TypeError
            label = k.split("-")[0]
            value = "-".join(k.split("-")[1:])
            index_name = df.index.name
            dict[value] = df
        df = pd.concat(dict, names=[label, index_name])
    except TypeError:
        dict = {}
        for k, v in runs.items():
            label = k.split("-")[0]
            value = "-".join(k.split("-")[1:])
            dict[value] = get_dataframe(v)
            index_names = dict[value].index.names
        df = pd.concat(dict, names=[label]+index_names)
    return df


def plot_convergence():
    df = combine_dataframes(lambda run: run.default.scf.convergence)
    last = get_last_iteration(df).reset_index("#iter")
    with PdfPages("convergence.pdf") as pdf:
        width = 5
        # plot comparison of systems for each parameter combination
        grouped = df.groupby(df.index.names[:-2])
        for i, (k, v) in enumerate(grouped):
            f, ax = plt.subplots(1, 1, figsize=(width, width*0.6), sharex=True)
            ax.set_title(k)
            for k2, v2 in v.groupby(df.index.names[-2]):
                ax.semilogy(v2.index.get_level_values("#iter"),
                            v2["rel_dens"], label=k2)
            if len(v.groupby(df.index.names[-2])) < 8:
                ax.legend()
            ax.set_ylabel("Relative density change")
            ax.set_xlabel("Iteration number")
            f.tight_layout()
            pdf.savefig()
            plt.close()

        # plot comparison of parameter combinations for each system
        grouped = df.groupby(df.index.names[-2])
        for i, (k, v) in enumerate(grouped):
            f, ax = plt.subplots(1, 1, figsize=(width, width*0.6), sharex=True)
            ax.set_title(k)
            for k2, v2 in v.groupby(df.index.names[:-2]):
                ax.semilogy(v2.index.get_level_values("#iter"),
                            v2["rel_dens"], label=k2)
            ax.legend()
            ax.set_ylabel("Relative density change")
            ax.set_xlabel("Iteration number")
            f.tight_layout()
            pdf.savefig()
            plt.close()

        # last["#iter"].groupby(df.index.names[:-2]).boxplot()
        # pdf.savefig()
        # plt.close()


if __name__ == "__main__":
    plot_convergence()
